import os
import re
from glob import iglob

from setuptools import setup, find_packages

version = ''
with open('license/__init__.py') as f:
    # noinspection PyRedeclaration
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]', f.read(), re.MULTILINE).group(1)

if (not version):
    raise RuntimeError("Package version is not set")

if (version.endswith(('a', 'b', 'rc'))):
    # append version identifier based on commit count
    try:
        import subprocess
        p = subprocess.Popen(['git', 'rev-list', '--count', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if out:
            version += out.decode('utf-8').strip()
        p = subprocess.Popen(['git', 'rev-parse', '--short', 'HEAD'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if out:
            version += '+g' + out.decode('utf-8').strip()
    except Exception:
        pass

readme = ''
with open('README.rst') as f:
    # noinspection PyRedeclaration
    readme = f.read()

setup \
(
    name = 'license-win',
    url = 'https://github.com/Hares-Lab/license',
    version = version,
    packages = [ p for p in find_packages() if p != 'test' ],
    author = 'Peter Zaitcev / USSX-Hares',
    author_email = 'ussx-hares@yandex.ru',
    license = 'MIT',
    description = "Library that encapsulates free software licenses",
    long_description = readme,
    long_description_content_type = 'text/x-rst',
    include_package_data = True,
    setup_requires = [ 'wheel' ],
    install_requires = [ 'jinja2', 'pytest' ],
    test_requires = [ 'pytest' ],
    extras_require = { 'test': [ 'pytest' ] },
    classifiers =
    [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Libraries',
    ]
)
